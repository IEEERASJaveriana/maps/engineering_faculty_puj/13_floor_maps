# Comparativa de los algoritmos de SLAM en ROS1: Gmapping, hector y karto.

### Integrantes: 

* Jorge Torrado 
* Carlos Cortés 
* Sebastián Clavijo

En este proyecto se emplearon los paquetes para ROS noetic de
turtlebot3_slam, turtlebot3_navigation y map_server

## Pasos realizados para obtención de mapas y navegación:

1\. Configuracion del bashrc del pc master y del turtlebot3, de acuerdo
a como se explica en la siguiente documentación:
https://emanual.robotis.com/docs/en/platform/turtlebot3/sbc_setup/#sbc-setup

2\. Conexión a turtlebot3 mediante el comando: 
    
    ssh ubuntu@{ip del turtlebot}

3\. Inicio del nodo maestro con el comando: 

    roscore

4\. Ejecución del bringup (en la misma terminal donde se realizó la
conexión mediante ssh del paso 2) con el comando: 

    roslaunch turtlebot3_bringup turtlebot3_robot.launch

5\. Ejecución de slam con el comando: 

    roslaunch turtlebot3_slam turtlebot3_slam.launch

6\. Ejecución del nodo de teleoperación con el comando: 

    roslaunch turtlebot3_teleop turtlebot3_teleop_key.launch

7\. Luego se mueve el robot para que vaya reconociendo el entorno y
generando el mapa deseado. Al estar terminado el mapa se guarda con este
comando: 
    rosrun map_server map_saver -f ~/{dirección en la que se va a guardar, debe terminar con /"nombre del mapa"}

Al ya disponer del mapa, se puede realizar el \"piloto automático\"
(primero se requiere haber ejecutado los pasos 1 al 4) al correr el nodo
de navegacion de la siguiente manera: 
    roslaunch turtlebot3_navigation turtlebot3_navigation.launch map_file:={todo el path hasta el archivo yaml del mapa, se inicia siempre con \$HOME/ para usuarios de ubuntu}

## Resultados y Conclusiones:

Se obtuvieron como resultados mapas del pasillo del piso 13 del
laboratorio de robótica de la Pontificia Universidad Javeriana con los
algoritmos gmapping, karto y Hector, siendo este último el más preciso y
el que se empleó para realizar un mapa completo del piso 13, tanto del
pasillo de este piso como del salón de robótica ubicado en el mismo.

Los mapas realizados con algoritmos fuertemente basados en odometría
como gmapping y karto, terminan siendo ligeralmente erróneos con
respecto al mapa real, además de contener mucho ruido (lineas solapadas,
puntos por fuera del mapa, etc.), esto debido a que los algoritmos
basados en odometría estiman distintas ubicaciones posibles, toman las
perspectivas de dichas ubicaciones y las solapan en un mismo mapa,
siendo esto muy útil para entornos complejos como las zonas exteriores,
pero resultando poco confiable para entornos más simples como lo son las
zonas interiores.

El mapa realizado con el algoritmo Hector, que no se basa en odometría,
resulta ser más preciso y muy acorde a la realidad, esto debido al hecho
de que no se basa en odometría y, por el contrario, usa estimación
mediante cuadrículas (marca como ocupado o desocupado cada cuadro
dependiendo de si hay o no objetos en dichos cuadros), lo cual permite
mayor precisión en entornos simples como las zonas interiores, mientras
que resulta inefectivo en zonas complejas como los exteriores (este
algoritmo es todo lo opuesto a gmapping y karto).
